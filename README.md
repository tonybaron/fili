# 接口文档

测试域名：

```
https://webet.17fm.cc/
```

## 1 注册登录

### 1.验证码

URI `/api/captcha`  
Method **GET**  
返回 cookie: SESSION  
返回内容类型: image/jpeg  
返回内容：验证码图片

### 2.注册

URI `/api/register`  
Method **POST**  
发送 cookie: SESSION  
发送 json:

```json
{
  "user": "abcg@def.com", // 用户名
  "pwd": "123456", // 密码
  "refer": "92j301", // 推荐码，若没有可为null
  "captcha": "u3gk" // 验证码
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": null
}
```

注册成功后，自动进入已登录状态

### 3.登录

URI `/api/login`  
Method **POST**  
发送 cookie: SESSION  
发送 json:

```json
{
  "user": "abcg@def.com", // 用户名
  "pwd": "123456", // 密码
  "captcha": "u3gk" // 验证码
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": null
}
```

### 4.退出

URI `/api/logout`  
Method **POST**  
发送 cookie: SESSION

返回 cookie: 删除 SESSION
返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": null
}
```

### 5.改密码

URI `/api/password/reset`  
Method **POST**  
发送 cookie: SESSION
发送 json:

```json
{
  "pwd0": "123456", // 旧密码
  "pwd": "654321" // 新密码
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": null
}
```

### 6.忘记密码

URI `/api/password/forget`  
Method **POST**  
发送 json:

```json
{
  "account": "xxxx@gmail.com",
  "redirect_url": "https://www.xxxx.com/renew_password.html"
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": null
}
```

执行成功会给用户发送一封邮件，包含的链接会以 redirect_url+"?token=xxxx" 的方式重新拼接

### 7.重设密码

URI `/api/password/renew`  
Method **POST**  
发送 json:

```json
{
  "token": "e2675bd7-8f2e-45c0-84e0-448b7b4019b4",
  "pwd": "12345678"
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": null
}
```

重设密码之后，token 随即失效

## 2 API

### 1.个人信息

URI： `api/profile`  
Method： **POST**  
发送 cookie： SESSION

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": {
    "id": 24, // 用户ID
    "account": "abcg@def.com", // 登录账号
    "level": 0, // VIP等级
    "balance": "0.00", // 余额
    "refer": "7yxy18" // 推荐码
  }
}
```

### ~~2. 首页-banner~~

URI： `api/index/banners"`  
Method： **POST**  
返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": [
    {
      "src": "https://aaaaaa.png",
      "link": "https://aaaaaa/game1"
    },
    {
      "src": "https://bbbbbb.png",
      "link": "https://bbbbbb/game2"
    }
  ]
}
```

### 2.首页

URI： `api/index"`
Method： **POST**
返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": [
    {
      "src": "https://aaaaaa.png",
      "link": "https://aaaaaa/game1"
    },
    {
      "src": "https://bbbbbb.png",
      "link": "https://bbbbbb/game2"
    }
  ]
}
```

### 3. 首页-大厅和游戏

URI： `api/index/games`  
Method： **POST**  
发送 json:

```json
{
  "limit": 9
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": [
    {
      "room_id": 1,
      "room": "In-House",
      "cover": "https://aaaaaa.png",
      "pages": 1,
      "list": [
        {
          "id": 1,
          "name": "crash",
          "cover": "https://bbbbbb.png"
        }
      ]
    }
  ]
}
```

### 游戏分页

URI： `api/index/room_games`  
Method： **POST**  
发送 json:

```json
{
  "room_id": 5,
  "page": 2,
  "limit": 9
}
```

返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": [
    {
      "id": 6,
      "name": "Dragon & Tiger",
      "cover": "https://webet-admin.17fm.cc/upload/1685181287562_26534.png"
    },
    {
      "id": 7,
      "name": "Dice",
      "cover": "https://webet-admin.17fm.cc/upload/1685181339561_17875.png"
    },
    {
      "id": 8,
      "name": "7 UP-DOWN",
      "cover": "https://webet-admin.17fm.cc/upload/1685181393609_2848.png"
    },
    {
      "id": 9,
      "name": "Lucky Number",
      "cover": "https://webet-admin.17fm.cc/upload/1685181433821_8849.png"
    }
  ]
}
```

### 首页-游戏链接

URI `api/game?id=1&lang=zh-CN`  
Method： **GET**  
发送 cookie： SESSION (可选)  
发送 Referer  
返回 302
参数说明：  
最后的数字为游戏 ID，参考接口`api/index/games`返回的 data[m].list[n].id
lang 用户选择的语言 简中 zh-CN 英文 en-US 不填则默认为英文
如果不发送 SESSION，则会以游客方式进入游戏

### 4.分享-邀请

URI： `api/share/invite`  
Method： **POST**  
发送 cookie： SESSION

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": {
    "total_invite": 24, // 总邀请人数
    "total_income": "2500.00", // 总的邀请奖励和反水
    "invited_today": 1, // 今天邀请人数
    "income_today": "300.00", // 今天的工资和提成
    "salary_estimate": "1500.00", // 当月的 邀请奖励+邀请任务奖励
    "last_month_salary": "13000.00", // 上个月的邀请奖励+邀请任务奖励
    "invited_bonus_month": "300.00", // 当月邀请奖励
    "commission_month": "653.45" // 当月反水
  }
}
```

### 5.分享-统计

URI： `api/share/statistics`  
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
    "begin": "20230215", // 开始日期
    "end": "20230424"   // 截止日期
}

或
{
  "day": 7 // 按最近7天
}
```

如果没有参数，默认返回 day=1 的数据  
如果时间段和天数都有，只返回时间段数据

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": {
    "invited": [2, 7, 16], // 被邀请人数 三级的
    "deposit": [1, 3, 9], // 有存款的人数 三级的
    "recharge": ["48300.00", "257800.00", "6681900.00"], // 总存款额
    "withdraw": ["1100.00", "1200.00", "1300.00"], // 总取款额
    "betAmount": ["2100.00", "2200.00", "2300.00"], // 总投注额
    "profits": ["3100.00", "3200.00", "3300.00"] // 总盈利
  }
}
```

### ~~6.分享-薪水~~

URI： `api/share/salary`  
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
    "begin": "20230215", // 开始日期
    "end": "20230424"   // 截止日期
}

或
{
  "day": 7 // 按最近7天
}
```

如果没有参数，默认返回 day=1 的数据  
如果时间段和天数都有，只返回时间段数据

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": "88800.00" // 某时间段的薪水
}
```

### 7.分享-下级存款和投注额

URI： `api/share/childlog`  
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "page": 1, // 第一页
  "limit": 20 // 每页20条数据
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": [
    {
      "date": "2023-03-14",
      "user": "abc@gmail.com",
      "deposit": "200.00",
      "bet": "300.00"
    }
  ],
  "count": 1 // 数据的总条数，可用来计算是否还有下一页
}
```

### 8.分享-下级奖励

URI： `api/share/bonus`  
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "page": 1, // 第一页
  "limit": 20, // 每页20条数据
  "type": "all" // all 或 invite 或 commission 默认为all
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": [
    {
      "date": "2023-04-15",
      "user": "abc@gmail.com",
      "amount": "500.00"
    }
  ],
  "count": 1 // 数据的总条数，可用来计算是否还有下一页
}
```

### 9. Bonus-活动详情

URI： `api/bonus/index`  
Method： **POST**
返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": [
    {
      "cover": "https://xxxx/123.png",
      "article_id": 1 // 没有文章，则没有该字段
    }
  ]
}
```

### 10.文章详情

URI： `api/article`
Method： **POST**
发送 json:

```json
{
  "article_id": 1
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": {
    "title": "我是标题",
    "txt": "<p>我是正文</p><a href=\"https://xxxx/\">Recharge</a>"
  }
}
```

### 11. VIP-个人数据

URI： `api/vip/index`  
Method： **POST**  
发送 cookie： SESSION  
返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": {
    "avatar": "123123123.png", // 头像
    "account": "abc@123.com", // 账号
    "next_level": 3, // 下一级别
    "bet": ["600", "30000", "17%"], // 升级至下一级 已投注  需要总投注 进度
    "deposit": ["0", "1000", "58%"], // 已用免费提现额度 总免费提现额度 比例
    "balance": "89,712.00", // 余额
    "withdrawable": "2,112.00" // 可体现额度
  }
}
```

### 12. VIP-充值渠道列表

URI： `api/recharge/list`  
Method： **POST**  
发送 cookie： SESSION  
返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": [
    {
      "id": 1,
      "name": "GCASH",
      "logo": "https://xxxx/xxx.png",
      "limit_min": "100.00",
      "limit_max": "100000.00"
    },
    {
      "id": 2,
      "name": "MAYA",
      "logo": "https://xxxx/xxx.png",
      "limit_min": "100.00",
      "limit_max": "100000.00"
    }
  ]
}
```

### 13. VIP-充值

URI： `api/recharge`  
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "id": 2, // 这里的ID对应 api/recharge/list 返回的 data[n].id
  "amount": "1000.00"
}
```

返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": {
    "redirect_url": "https://phpay.gazer8.info/api/payment_direct/redirect/GP100003873492"
  }
}
```

redirect_url 是用户充值的页面地址

### 14. VIP-提现主页

URI： `api/withdraw/index`  
Method： **POST**  
发送 cookie： SESSION  
返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": {
    "balance": "1000.00",
    "cards": [
      // 我已添加的提现账号
      {
        "id": 1,
        "bank_name": "GCash",
        "logo": "https://xxxx/xxx.png",
        "holder": "James",
        "card_num": "158796250",
        "whatsapp": "xxxxx"
      },
      {
        "id": 2,
        "bank_name": "Maya",
        "logo": "https://xxxx/xxx.png",
        "holder": "James",
        "card_num": "158796250",
        "whatsapp": "zzzzz"
      }
    ],
    "fee": "2.5%",
    "level": 0,
    "withdrawable": "109.00"
  }
}
```

### xx. VIP-提现渠道列表

URI： `api/withdraw/list`  
Method： **POST**  
发送 cookie： SESSION  
返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": [
    {
      "id": 1,
      "name": "GCASH",
      "logo": "https://xxxx/xxx.png",
      "limit_min": "100.00",
      "limit_max": "100000.00",
      "is_bank": 0 // 是否为银行渠道， 0-商业机构 1-银行
    },
    {
      "id": 2,
      "name": "MAYA",
      "logo": "https://xxxx/xxx.png",
      "limit_min": "100.00",
      "limit_max": "100000.00",
      "is_bank": 1
    }
  ]
}
```

### 14. VIP-添加提现账号

URI： `api/withdraw/add_card`  
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "withdraw_id": 1, // 这里的ID对应 api/withdraw/list 返回的 data[n].id
  "holder": "James", // 持卡人姓名
  "card_num": "158796250", // 手机号 或 银行卡号
  "whatsapp": "xxxxx"
}
```

返回 json:

```json
{
  "code": 0,
  "msg": "OK"
}
```

### xx. 编辑提现账号

暂未实现

### xx. 删除提现账号

URI： `api/withdraw/del_card`  
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "card_id": 1 // 对应api/withdraw/index返回的data.cards[n].id
}
```

返回 json:

```json
{
  "code": 0,
  "msg": "OK"
}
```

### 15. VIP-提交提现申请

URI： `api/withdraw`  
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "card": 2, // 对应api/withdraw/index返回的data.cards[n].id
  "amount": "1000.00"
}
```

返回 json:

```json
{
  "code": 0,
  "msg": "OK"
}
```

### 16. VIP-等级规则

URI： `api/vip/levels`
Method： **POST**  
发送 cookie： SESSION  
返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": [
    {
      "level": 0,
      "target": "0.00",
      "reward": "0.00",
      "fee": 0.025,
      "free_out": "5.00",
      "bet_back": 0.0
    },
    {
      "level": 1,
      "target": "10.00",
      "reward": "0.05",
      "fee": 0.025,
      "free_out": "5.00",
      "bet_back": 0.002
    }
  ]
}
```

### 17. 记录-充值记录

URI： `api/log/recharge`
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "page": 1, // 第一页
  "limit": 20, // 每页20条数据
  "begin": "20230101",
  "end": "20230531"
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": [
    {
      "id": 1,
      "amount": "1.00",
      "status": 4,
      "date": "2023-04-29"
    },
    {
      "id": 3,
      "amount": "3.00",
      "status": 5,
      "date": "2023-04-29"
    }
  ],
  "count": 2 // 数据的总条数，可用来计算是否还有下一页
}
```

status 数值：0-申请中 1-转入中 2-成功 3-失败 4-已通过 5-已拒绝 6-转出中

### 18. 记录-提现记录

URI： `api/log/withdraw`
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "page": 1, // 第一页
  "limit": 20, // 每页20条数据
  "begin": "20230101",
  "end": "20230531"
}
```

返回 json:

```json
{
  "code": 0, // 0表示成功， 其它为失败，失败原因参考msg
  "msg": "OK",
  "data": [
    {
      "id": 1,
      "amount": "1.00",
      "status": 4,
      "date": "2023-04-29"
    },
    {
      "id": 3,
      "amount": "3.00",
      "status": 5,
      "date": "2023-04-29"
    }
  ],
  "count": 2 // 数据的总条数，可用来计算是否还有下一页
}
```

status 数值：0-申请中 1-转入中 2-成功 3-失败 4-已通过 5-已拒绝 6-转出中

### 19. 记录-投注记录

URI： `api/log/bet`
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "page": 1, // 第一页
  "limit": 20, // 每页20条数据
  "begin": "20230101",
  "end": "20230531",
  "game_id": 1 // 0-全部 1-crash 2-mine 3-dice
}
```

返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": [
    {
      "id": 10071,
      "game": "crash",
      "amount": "1.00",
      "odd": 1.57,
      "bonus": "1.57",
      "status": 2,
      "date": "2023-05-13"
    }
  ],
  "count": 1 // 数据的总条数，可用来计算是否还有下一页
}
```

status 数值：0-未开 1-输 2-赢

### 20. 记录-奖励记录

URI： `api/log/bonus`
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "page": 1, // 第一页
  "limit": 20, // 每页20条数据
  "begin": "20230101",
  "end": "20230531"
}
```

返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": [
    {
      "id": 13,
      "amount": "1.00",
      "type": 5,
      "date": "2023-05-01"
    },
    {
      "id": 16,
      "amount": "1.00",
      "type": 5,
      "date": "2023-05-01"
    }
  ],
  "count": 1 // 数据的总条数，可用来计算是否还有下一页
}
```

status 数值：5-返奖 6-反水 7-推广任务奖励 8-游戏任务奖励 9-邀请平台奖励

### 21. 短消息

URI： `api/vip/messages`
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "page": 1, // 第一页
  "limit": 20 // 每页20条数据
}
```

返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": [
    {
      "id": 1,
      "title": "我是标题",
      "txt": "我是正文",
      "datetime": "2023-05-13 09:19:15"
    }
  ],
  "count": 1 // 数据的总条数，可用来计算是否还有下一页
}
```

### 22. 收藏

URI： `api/favs`
Method： **POST**  
发送 cookie： SESSION  
返回 json:

```json
{
  "code": 0,
  "msg": "OK",
  "data": [
    {
      "id": 1,
      "name": "11",
      "cover": "111",
      "href": ""
    },
    {
      "id": 2,
      "name": "22",
      "cover": "22",
      "href": "222"
    }
  ]
}
```

### 23. 添加收藏

URI： `api/favs/add`
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "game_id": 1
}
```

game_id 对应接口`/api/index/games`返回的 data[m].list[n].id  
返回 json:

```json
{
  "code": 0,
  "msg": "OK"
}
```

### 24. 取消收藏

URI： `api/favs/remove`
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "fav_id": 1
}
```

fav_id 对应接口`/api/favs`返回的 data[n].id  
返回 json:

```json
{
  "code": 0,
  "msg": "OK"
}
```

### 25. 心跳包

URI： `api/trace`  
Method： **POST**  
发送 cookie： SESSION  
返回 json:

```json
{
  "code": 0,
  "msg": "OK"
}
```

## 3 游戏 WebSocket

### 0.心跳包

发送

```json
{
  "cmd": "echo",
  "seq": 1001, // 自定义的包序列号，可用累加计数器生成，可用于诊断上行和下行的网络延迟
  "t1": 1683260485635 // 客户端发送的时间戳 毫秒
}
```

返回

```json
{
  "cmd":"echo",
  "seq": 1001, // 客户端发送的包序列号
  "t1": 1683260485635 // 客户端发送的时间戳 毫秒
  "t2": 1683260486171 // 服务端发送的时间戳 毫秒
}
```

### 1.crash

游戏分别有 4 种状态轮询，betting 10 秒 waiting 2 秒 raising n 秒 crashed 3 秒  
ws 接口 json 数据字段含义如下：  
state: 当前状态  
t: 当前状态的剩余时间(毫秒)  
round: 当前第几轮  
m: 当前赔率/逃跑时的赔率  
n: 爆炸的赔率  
amount: 投注额  
balance: 当前可用余额  
escaped: 是否已逃跑

- #### ws 接口

```http request
/game/crash
```

- #### 消息
  进入房间会收到初始消息

```json
{
  "state": "betting",
  "t": 9999,
  "balance": "12.61",
  "amount": "0.00",
  "escaped": false,
  "round": 765286
}
```

投注开始消息

```json
{
  "state": "betting",
  "t": 10000
}
```

等待开始消息

```json
{
  "state": "waiting",
  "t": 2000
}
```

飞行消息

```json
{
  "state": "raising",
  "m": 2.400266
}
```

玩家逃跑消息

```json
{
  "state": "escape",
  "user": "abc@def.com"
}
```

爆炸消息

```json
{
  "state": "crash",
  "n": 34.192616,
  "t": 3000
}
```

输的结算消息

```json
{
  "result": "lose",
  "m": "1.05",
  "n": "5.98",
  "amount": "1.00",
  "bonus": "0.00",
  "balance": "12.66",
  "round": 765286
}
```

- #### 投注
  **发送**:

```json
{
  "cmd": "bet",
  "amount": "1"
}
```

**返回**：

```json
{
  "cmd": "bet",
  "code": 0,
  "msg": "ok",
  "amount": "1.00",
  "balance": "11.61"
}
```

- #### 逃脱
  **发送**:

```json
{
  "cmd": "escape"
}
```

**返回**：

```json
{
  "cmd": "escape",
  "code": 0,
  "msg": "ok",
  "m": "1.05",
  "result": "win",
  "amount": "1.00",
  "bonus": "1.05",
  "balance": "11.61"
}
```

### 2.mine

- #### ws 接口

```http request
/game/mine
```

- #### 消息
  进入房间会收到初始消息

```json
{
  "balance": "12.61",
  "risk": 1,
  "hits": [
    1.03, 1.04, 1.05, 1.05, 1.06, 1.07, 1.07, 1.08, 1.09, 1.1, 1.12, 1.14, 1.16, 1.19, 1.23, 1.28,
    1.34, 1.44, 1.59, 1.82, 2.24, 3.06, 5, 23
  ]
}
```

- #### 选择 Risk
  **发送**:

```json
{
  "cmd": "risk",
  "risk": "2"
}
```

**返回**：

```json
{
  "cmd": "risk",
  "code": 0,
  "msg": "ok",
  "risk": 2,
  "hits": [
    1.03, 1.12, 1.23, 1.35, 1.5, 1.66, 1.86, 2.09, 2.37, 2.71, 3.13, 3.65, 4.31, 5.18, 6.33, 7.91,
    10.17, 13, 19, 28, 47, 95, 285
  ]
}
```

- #### 投注
  **发送**:

```json
{
  "cmd": "bet",
  "amount": "1"
}
```

**返回**：

```json
{
  "cmd": "bet",
  "code": 0,
  "msg": "ok",
  "amount": "1.00",
  "balance": "12.61",
  "level": 0,
  "round": 765286
}
```

- #### 挖雷
  **发送**:

```json
{
  "cmd": "dig",
  "position": [0, 3]
}
```

**成功返回**：

```json
{
  "cmd": "dig",
  "code": 0,
  "msg": "ok",
  "result": "success",
  "opened": 1
}
```

**或，爆炸返回**：

```json
{
  "cmd": "dig",
  "code": 0,
  "msg": "ok",
  "result": "fail",
  "m": "0",
  "risk": 2,
  "opened": 2,
  "amount": "2.00",
  "bonus": "0.00",
  "balance": "12.66",
  "round": 765286,
  "map": [
    [0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0]
  ]
}
```

**或，最后一个宝箱**：

```json
{
  "cmd": "dig",
  "code": 0,
  "msg": "ok",
  "result": "win",
  "m": "1.05",
  "risk": 2,
  "opened": 2,
  "amount": "2.00",
  "bonus": "2.10",
  "balance": "12.66",
  "round": 765286,
  "map": [
    [0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0]
  ]
}
```

- #### 结算
  **发送**:

```json
{
  "cmd": "cashout"
}
```

**返回**：

```json
{
  "cmd": "cashout",
  "code": 0,
  "msg": "ok",
  "m": "1.05",
  "risk": 2,
  "opened": 2,
  "amount": "2.00",
  "bonus": "2.10",
  "balance": "12.66",
  "round": 765286,
  "map": [
    [0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0]
  ]
}
```

### 3.dice

- #### ws 接口

```http request
/game/dice
```

- #### 消息
  进入房间会收到初始消息

```json
{
  "balance": "12.61",
  "odds": [
    98, 49, 32.6667, 24.5, 19.6, 16.3333, 14, 12.25, 10.8889, 9.8, 8.9091, 8.1667, 7.5385, 7,
    6.5333, 6.125, 5.7647, 5.4444, 5.1579, 4.9, 4.6667, 4.4545, 4.2609, 4.0833, 3.92, 3.7692,
    3.6296, 3.5, 3.3793, 3.2667, 3.1613, 3.0625, 2.9697, 2.8824, 2.8, 2.7222, 2.6486, 2.5789,
    2.5128, 2.45, 2.3902, 2.3333, 2.2791, 2.2273, 2.1778, 2.1304, 2.0851, 2.0417, 2, 1.96, 1.9216,
    1.8846, 1.8491, 1.8148, 1.7818, 1.75, 1.7193, 1.6897, 1.661, 1.6333, 1.6066, 1.5806, 1.5556,
    1.5313, 1.5077, 1.4848, 1.4627, 1.4412, 1.4203, 1.4, 1.3803, 1.3611, 1.3425, 1.3243, 1.3067,
    1.2895, 1.2727, 1.2564, 1.2405, 1.225, 1.2099, 1.1951, 1.1807, 1.1667, 1.1529, 1.1395, 1.1264,
    1.1136, 1.1011, 1.0889, 1.0769, 1.0652, 1.0538, 1.0426, 1.0316
  ]
}
```

- #### 投注
  **发送**:

```json
{
  "cmd": "bet",
  "amount": "1",
  "type": 1, // 1表示猜比庄小 2表示猜比庄大
  "point": 50
}
```

**返回**：

```json
{
  "cmd": "bet",
  "code": 0,
  "msg": "ok",
  "amount": "1.00",
  "balance": "12.61",
  "level": 0,
  "round": 765286,
  "result": "win", // win 或 lose
  "type": 1,
  "point": 50,
  "result_point": 49,
  "odd": 1.96,
  "bonus": "1.96"
}
```

### 4.plinko(暂未实现)

## 4 测试接口

### 1.充值

URI： `/api/test/deposit`  
Method： **POST**  
发送 cookie： SESSION  
发送 json:

```json
{
  "amount": "1000.00"
}
```
