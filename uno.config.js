import { defineConfig, presetAttributify, presetUno, presetIcons } from 'unocss'

export default defineConfig({
  presets: [
    presetAttributify({
      /* preset options */
    }),
    presetUno(),
    presetIcons({
      collections: {
        flag: () => import('@iconify-json/flag/icons.json').then((i) => i.default),
        ep: () => import('@iconify-json/ep/icons.json').then((i) => i.default),
        mdi: () => import('@iconify-json/mdi/icons.json').then((i) => i.default)
        // twemoji has some problem
        // twemoji: () => import('@iconify-json/twemoji/icons.json').then(i => i.default),
      },
      scale: 1.4,
      warn: true
    })
  ],
  theme: {
    colors: {
      // 橘黄色老板面暂时放弃使用
      active: '#ffd991',
      headerBg: '#1f242a',
      tableBg: '#1f262e',
      normalBg: '#101419',
      border: {
        login: '#5a636d'
      },
      // 蓝色版面颜色变量
      tay: {
        bg: {
          0: '#1F2833',
          1: '#16202d',
          2: '#18202c',
          3: '#111A23',
          4: '#1C2632',
          5: '#161f2c',
          6: '#385276',
          7: '#2A3646',
          8: '#00143E',
          9: '#151C26',
          10: '#142954',
          11: '#233857',
          12: '#202C3C',
          13: '#325DB5',
          14: '#003274',
          15: '#0A1C33',
          16: '#252E3A',
          17: '#212A39',
          18: '#141E2A',
          19: '#576579',
          20: '#152C67'
        },
        active: {
          1: '#2283f6',
          2: '#dd234b',
          3: '#EC1D49',
          4: '#CE3030'
        },
        gary: {
          1: '#1d2532'
        },
        text: {
          1: '#93acd3',
          2: '#55657e',
          3: '#595e63',
          4: '#FFC459',
          5: '#FCE907',
          6: '#999999',
          7: '#FBAB1C',
          8: '#233857',
          9: '#32D850',
          10: '#00A650',
          11: '#999999',
          12: '#56667F',
          13: '#EF891A',
          14: '#94A8CD',
          15: '#ffecc5',
          16: '#8F9FB9'
        }
      }
    }
  }
})
