import 'virtual:uno.css'
import '@unocss/reset/normalize.css'
import './assets/css/main.scss'
import 'element-plus/theme-chalk/dark/css-vars.css'
import 'element-plus/es/components/message/style/css'
import '@/utils/root.js'

// import errorImg from '@img/common/error.png'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createI18n } from 'vue-i18n'
import messages from '@intlify/unplugin-vue-i18n/messages'

import App from './App.vue'
import router from './router'
import VueClipboard from 'vue3-clipboard'
import { MotionPlugin } from '@vueuse/motion'
import { Lazyload } from 'vant'
import 'vant/es/toast/style'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const i18n = createI18n({
  legacy: false,
  locale: localStorage.lang || 'zh-CN',
  fallbackLocale: 'en',
  messages
})



const app = createApp(App)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

/* 公共方法 */
import commonV from "@/common/index.ts";
app.config.globalProperties.commonV = commonV

app
  .use(createPinia())
  .use(router)
  .use(i18n)
  .use(MotionPlugin)
  .use(VueClipboard, {
    autoSetContainer: true
  })
  .use(Lazyload, {
    // loading: loadingImg,
    // error: errorImg,
    lazyComponent: true
  })
  .mount('#app')
