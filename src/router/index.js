import { createRouter, createWebHashHistory } from 'vue-router'
import { useSystemStore } from '../stores/system'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Index',
      component: () => import('@/views/IndexView.vue'),
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'Home',
          component: () => import('@/views/HomeView.vue')
        },
        {
          path: '/record',
          name: 'Record',
          component: () => import('@/views/RecordView.vue')
        },
        {
          path: '/bonus',
          name: 'Bonus',
          component: () => import('@/views/BonusView.vue')
        },
        {
          path: '/my',
          name: 'My',
          component: () => import('@/views/MyView.vue')
        },
        {
          path: '/share',
          name: 'Share',
          component: () => import('@/views/ShareView.vue')
        },
        {
          path: '/recharge',
          name: 'Recharge',
          redirect: '/',
          component: () => import('@/views/RechargeView.vue')
        },
        {
          path: '/terms',
          name: 'Terms',
          component: () => import('@/views/TermsView.vue')
        },
        {
          path: '/article',
          name: 'Article',
          component: () => import('@/views/ArticleView.vue')
        },
        {
          path: '/download',
          name: 'Download',
          component: () => import('@/views/DownloadView.vue')
        },
        {
          path: '/vip',
          name: 'Vip',
          component: () => import('@/views/vip.vue')
        },
        {
          path: '/mines',
          name: 'Mines',
          component: () => import('@/views/MinesView.vue')
        },
        {
          path: '/dice',
          name: 'Dice',
          component: () => import('@/views/DiceView.vue')
        },
        {
          path: '/collect',
          name: 'Collect',
          component: () => import('@/views/CollectView.vue')
        },
        {
          path: '/cards',
          name: 'Cards',
          component: () => import('@/views/cards.vue')
        },
        {
          path: '/account',
          name: 'Account',
          component: () => import('@/views/AccountView.vue')
        },
        {
          path: '/crash',
          name: 'Crash',
          component: () => import('@/views/CrashView.vue')
        },
        {
          path: '/records',
          name: 'Records',
          component: () => import('@/views/RecordsView.vue')
        },
        {
          path: '/messages',
          name: 'Message',
          component: () => import('@/views/MessageView.vue')
        }
      ]
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    // 始终滚动到顶部
    return { top: 0 }
  }
})

router.beforeEach(async (to, from) => {
  console.log('路由拦截', to, from)
  const system = useSystemStore()
  console.log('store', system, to)
  if (!system.isLogin && to.path != '/home') {
    system.loginPop.type = 'login'
    system.loginPop.show = true
    return {
      path: '/'
    }
  }
})

export default router
