const manytomanyarrData = [{
    // 人员多选
    paramtype: 'chooseuser',
    clickway: "chooseuser",
    key: "real_name",
    comname: 'choseuserData'
}, {
    // 科室
    paramtype: 'choosedepartment',
    clickway: "choosedepartment",
    key: "department_name"
}, {
    // 科室角色
    paramtype: 'chooserole',
    clickway: "chooserole",
    key: "department_name"
}]

export default manytomanyarrData