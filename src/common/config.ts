import { createI18n } from 'vue-i18n'
import messages from '@intlify/unplugin-vue-i18n/messages'
const i18n = createI18n({
    legacy: false,
    locale: localStorage.lang || 'zh-CN',
    fallbackLocale: 'en',
    messages
})
// console.log(i18n.global.t);

//下拉配置项
const statusOption = {
    "status_data": [{
        value: 0,
        label: i18n.global.t('common.inapplication'),
    },
    {
        value: 1,
        label: i18n.global.t('common.transferring'),
    }, {
        value: 2,
        label: i18n.global.t('common.success'),
    }, {
        value: 3,
        label: i18n.global.t('common.fail'),
    }, {
        value: 4,
        label: i18n.global.t('common.passed'),
    }, {
        value: 5,
        label: i18n.global.t('common.rejected'),
    }, {
        value: 6,
        label: i18n.global.t('common.transferringout'),
    }],
    //数值
    Type_data: [{
        value: 0,
        label: i18n.global.t('common.noopen'),
    }, {
        value: 1,
        label: i18n.global.t('common.lose'),
    }, {
        value: 2,
        label: i18n.global.t('common.win'),
    }]


}

//弹窗配置项
const chooseComponent = {

}

export default {
    statusOption,
    chooseComponent
}