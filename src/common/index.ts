import config from './config'

//判断数组是数组且不为空
function ifrrandnotnone(arr: object[] = []) {
    return Object.prototype.toString.call(arr) === "[object Array]" && arr.length > 0
}
//日期格式化
function formatDate(date: number | string | Date, fmt: string): string {
    if (!date) {
        fmt = ""
        return fmt
    }
    date = new Date(date)
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(
            RegExp.$1,
            (date.getFullYear() + "").substr(4 - RegExp.$1.length)
        )
    }
    let o = {
        "M+": date.getMonth() + 1,
        "d+": date.getDate(),
        "h+": date.getHours() % 12 == 0 ? 12 : date.getHours() % 12, //小时
        "H+": date.getHours(), //小时
        "m+": date.getMinutes(),
        "s+": date.getSeconds()
    }
    for (let k in o) {
        if (new RegExp(`(${k})`).test(fmt)) {
            let str = o[k] + "";
            fmt = fmt.replace(
                RegExp.$1,
                RegExp.$1.length === 1 ? str : padLeftZero(str)
            )
        }
    }
    return fmt
}

//获取数组对照值
function matchOption(Option: param[], key: string, name?: string) {
    let label = ""
    let other_label = ''
    for (let index = 0; index < Option.length; index++) {
        const element = Option[index]
        if (key == element.value) {
            label = element.label
            other_label = element[name] || ''
        }
    }
    return name ? other_label : label
}

//补零
function padLeftZero(str: string) {
    return ("00" + str).substr(str.length)
}


export default {
    statusOption: config.statusOption,
    formatDate,
    ifrrandnotnone,
    matchOption
}