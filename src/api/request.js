import { BaseUrl } from './env.js'
import APIX from './index'
const baseURL =
  import.meta.env.PROD && window.location.host.includes('www')
    ? import.meta.env.VITE_BASE_URL_WWW
    : import.meta.env.VITE_BASE_URL
// 图形验证码
export const ApiCaptcha = (params = {}, responseType) => {
  return APIX.GET(params, `${BaseUrl}captcha`, responseType)
}
//首页游戏
export const ApiHomegame = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}index/games`)
}
//首页游戏分页
export const ApiHomeGamePage = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}index/room_games`)
}
//获取游戏链接
export const ApiGameUrl = (params = {}) => {
  // console.log(params,BaseUrl);
  //  return APIX.GET(params, `${BaseUrl}game`)
  return baseURL + 'api/game?id=' + params.id + '&lang=' + params.lang
}
//首页banner
export const ApiHomeBanner = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}index`)
}
//侧边栏
export const ApiSidebar = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}leftMenus`)
}
// 登录
export const ApiLogin = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}login`)
}
// 注册
export const ApiRegister = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}register`)
}
// 退出登录
export const ApiLogout = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}logout`)
}
// 个人信息
export const ApiProfile = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}profile`)
}
// 查询邀请详情
export const ApiGetInvite = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}share/invite`)
}
// 查询统计详情
export const ApiGetStatis = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}share/statistics`)
}
// 查询薪水
export const ApiGetSalary = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}share/salary`)
}
// 查询下级
export const ApiGetChild = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}share/childlog`)
}
// 查询奖金
export const ApiGetBonus = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}share/bonus`)
}
// 活动详情
export const ApiBonusIndex = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}bonus/index`)
}
// 文章详情
export const ApiArticle = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}article`)
}
// vip个人数据
export const ApiPersonalVip = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}vip/index`)
}
// 充值列表
export const ApiRechargeList = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}recharge/list`)
}
// 充值
export const ApiDeposit = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}recharge`)
}
// 新充值
export const ApiDepositNew = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}recharge/index`)
}
// 充值记录
export const ApiDepoLog = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}log/recharge`)
}
// 提款
export const ApiWidthLog = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}log/withdraw`)
}
// 提款
export const ApiWidthdraw = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}withdraw`)
}
// 提现限制
export const ApiWithdrawLimit = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}getVipInfo`)
}
// 提款列表
export const ApiWidthdrawList = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}withdraw/list`)
}
// 提款主页
export const ApiWidthdrawIndex = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}withdraw/index`)
}
// 添加提款账号
export const ApiWidthdrawAddCard = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}withdraw/add_card`)
}
// 删除提款账号
export const ApiWidthdrawDelCard = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}withdraw/del_card`)
}
// vip等级规则
export const ApiVipRule = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}vip/levels`)
}
// 投注记录
export const ApiBetLog = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}log/bet`)
}
// 奖励记录
export const ApiBonusLog = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}log/bonus`)
}
// 短消息
export const ApiMessage = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}vip/messages`)
}
// 收藏列表
export const ApiFavList = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}favs`)
}
// 添加收藏
export const ApiAddFavour = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}favs/add`)
}
// 取消收藏
export const ApiRemoveFavour = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}favs/remove`)
}
// 心跳
export const ApiTrace = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}trace`)
}
// 修改密码
export const ApiModifyPass = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}password/reset`)
}
// 忘记密码
export const ApiForgetPass = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}password/forget`)
}
// 重设密码
export const APIResetPass = (params = {}) => {
  return APIX.POST(params, `${BaseUrl}password/renew`)
}

//获取Bonus-活动详情

function APIBonusList(params = {}) {
  return APIX.POST(params, `${BaseUrl}bonus/index`)
}

function APIPostWay(api, params = {}) {
  return APIX.POST(params, `${BaseUrl}${api}`)
}
export default {
  APIBonusList,
  APIPostWay
}
