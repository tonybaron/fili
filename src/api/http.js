import axios from 'axios' // 引入axios
import { useSystemStore } from '@/stores/system.js'
import { ElMessage } from 'element-plus'

axios.defaults.withCredentials = true

const baseURL = import.meta.env.VITE_BASE_URL
// console.log(baseURL);
const instance = axios.create({
  baseURL: baseURL,
  timeout: 20000, // 请求超时时间
  withCredentials: true
})

// 请求
instance.interceptors.request.use(
  async (config) => {
    //   try {
    //     let Token = window.$app.$store.state.wallet.token
    //     config.headers['x-auth-token'] = Token || ''
    //   } catch (e) {
    //     console.warn('本次请求没得TOKEN')
    //   }
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

// 返回
instance.interceptors.response.use(
  async (res) => {
    const sysStore = useSystemStore()
    if (res.status === 200) {
      console.log('返回', res)
      switch (parseInt(res.data.code)) {
        case 401:
          sysStore.isLogin = false
          sysStore.reset()
          sysStore.loginPop.type = 'login'
          sysStore.loginPop.show = true
          return Promise.resolve(res.data)
        // case -1:
        //   sysStore.isLogin = false
        //   sysStore.reset()
        //   sysStore.loginPop.type = 'login'
        //   sysStore.loginPop.show = true
        //   return Promise.resolve(res.data)
        // case 405:
        //   // 未登录
        //   sysStore.isLogin = false
        //   return Promise.resolve(res.data)
        case 0:
          return Promise.resolve(res.data)
        default:
          if (res.data.type == 'image/jpeg') {
            return Promise.resolve(res.data)
          } else {
            ElMessage.error(res.data.msg)
            return false
          }
      }
    }
  },
  (error) => {
    console.log(error.response)
    ElMessage({
      message: 'network error!',
      type: 'warning'
    })
    console.warn(error)
  }
)

export const get = (params, url, responseType = 'json', self = false) => {
  console.log(url)
  return instance.get(url, { params, self, responseType })
}
export const post = (params, url, self = false) => {
  return instance.post(url, params, { self })
}
