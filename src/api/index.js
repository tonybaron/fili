import { get, post } from './http'

export default {
  POST(params = {}, link, self) {
    return new Promise((resolve) => {
      post(params, link, self).then((data) => {
        resolve(data)
      })
    })
  },
  GET(params = {}, link, self, responseType) {
    return new Promise((resolve) => {
      get(params, link, self, responseType).then((data) => {
        resolve(data)
      })
    })
  }
}
