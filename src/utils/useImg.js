// header
import logo from '@img/logo.png'
// sidebar
import imgBonus from '@img/header/bonus.png'
import imgCentro from '@img/header/centro.png'
import imgCrash from '@img/header/crash.png'
import imgColor from '@img/header/color.png'
import imgMines from '@img/header/mines.png'
import imgvip0 from '@img/vip/0.png'
import imgvip1 from '@img/vip/1.png'
import imgvip2 from '@img/vip/2.png'
import imgvip3 from '@img/vip/3.png'
import imgvip4 from '@img/vip/4.png'
import imgvip5 from '@img/vip/5.png'
import imgvip6 from '@img/vip/6.png'
import imgvip7 from '@img/vip/7.png'
import imgvip8 from '@img/vip/8.png'
import imgvip9 from '@img/vip/9.png'
import imgvip10 from '@img/vip/10.png'

export function useImg() {
  return {
    logo,
    imgBonus,
    imgCentro,
    imgCrash,
    imgColor,
    imgMines,
    imgvip0,
    imgvip1,
    imgvip2,
    imgvip3,
    imgvip4,
    imgvip5,
    imgvip6,
    imgvip7,
    imgvip8,
    imgvip9,
    imgvip10
  }
}
