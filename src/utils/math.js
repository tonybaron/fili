import { BigNumber } from "bignumber.js";

export function add(a, b) {
  const ab = new BigNumber(a);
  const bb = new BigNumber(b);
  return ab.plus(bb).toString();
}

export function minus(a, b) {
  const ab = new BigNumber(a);
  const bb = new BigNumber(b);
  return ab.minus(bb).toString();
}
