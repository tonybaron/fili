// 登录用户名校验
export const verifyLoginUsername = /^\w{2,20}$/
// 忘记密码问题答案2-20位中文
export const verifyForgetAnswer = /^[\u4e00-\u9fa5]{2,20}$/gi
// 推荐人用户名校验
export const verifyTuijianName = /^[aAUu0-9][0-9]{1,8}$/
// 登录密码校验
export const verifyLoginPassword = /^.{,20}$/
// 注册用户名校验
export const verifyRegUsername = /(?!^(\d+|[_-]+|[a-zA-Z]+|[~!@#$%^&*?}]+)$)^[\w~!@#$%^&*?]{6,20}$/
// 注册代理账号校验
export const verifyAgentName = /(?!^(\d+|[_-]+|[a-zA-Z]+|[~!@#$%^&*?}]+)$)^[\w~!@#$%^&*?]{6,20}$/
// 注册密码长度校验：长度为6-20位
export const verifyRegPasswordLen = /^.{6,20}$/
// 注册密码强长度校验：长度11-20位
export const verifyRegPasswordLong = /^[\w~!@#$%^&*?]{11,20}$/
// 注册密码规则校验： 至少包含1个数字和1个字母
export const verifyRegPasswordRule = /^(?=.*[0-9])(?=.*[a-zA-Z]).{6,20}$/
// 注册手机号校验
export const verifyRegPhone = /^1[3-9]\d{9}$/
// 注册qq校验
export const verifyRegQQ = /^\d{5,15}$/
// 注册微信校验
export const verifyRegWechat = /^[a-zA-Z0-9_-]{6,20}$/
// 注册skype校验
export const verifyRegSkype = /^[a-zA-Z0-9_\.\-\@]{3,30}$/
// 注册Line校验
export const verifyRegLine = /^\w{3,30}$/
// 注册facebook校验
export const verifyRegFackbook = /^\w{3,30}$/
// 注册邮箱校验
export const verifyRegEmail = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/
// 注册取款密码校验
export const verifyRegQkpass = /^[0-9]{6}$/
// 注册银行卡号
export const verifyRegBankcard = /^[0-9]{13,19}$/
// 开户行名称4-22中文
export const verifyKHH = /^[\u4e00-\u9fa5]{4,22}$/gi
// 开户行名称其他语种
export const verifyKHHForeign = /^.{1,22}$/gi
// 手机验证码
export const verifyMobilcode = /^\d{6}$/
// 真实姓名校验
export const verifyTruename = /^[\u4e00-\u9fa5]{2,20}$/gi
// 校验安全问题答案
export const verifyRegCustomAnster = /^[a-zA-Z0-9\u4e00-\u9fa5~!@#$%^&*?]{4,20}$/
