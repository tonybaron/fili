import { getCookie } from '@/utils/tool'
var ws = null
let lockReconnect = false // 避免重复连接
let wsUrl = ''

const wsdomain =
  import.meta.env.PROD && window.location.host.includes('www')
    ? import.meta.env.VITE_BASE_WS_WWW
    : import.meta.env.VITE_BASE_WS
var global_callback = null

export const createWebSocket = function (url, callback) {
  if (wsUrl) return
  console.log('session', getCookie('SESSION'))
  if (url && url.length < 10) {
    let session = 'NmEyZDQ3NTYtNjgxZS00ZDAwLTgxMjgtZmYwZDdlNDg4ZTI2'
    if (getCookie('SESSION')) {
      session = getCookie('SESSION')
    }
    console.log('session', session)
    console.log('wsdomain', wsdomain)
    console.log('url', url)
    wsUrl = `${wsdomain}${url}?SESSION=${session}`
    console.log('wsurl', wsUrl)
  }

  if (callback) {
    global_callback = callback
  }

  try {
    if ('WebSocket' in window) {
      if (!wsUrl) return
      ws = new WebSocket(wsUrl)
    } else {
      console.log('您的浏览器不支持websocket')
    }
    initEventHandle()
  } catch (e) {
    reconnect(wsUrl)
    console.log(e)
  }
}
export const sendWebsocket = function (data) {
  console.log('发送给ws', data)
  ws.send(JSON.stringify(data))
}
export const closeWebsocket = function () {
  if (ws) {
    ws.close()
    ws = null
    wsUrl = ''
    heartCheck.reset()
    console.log('断开链接')
  }
}

function initEventHandle() {
  ws.onclose = function () {
    reconnect(wsUrl)
    console.log('llws连接关闭!' + new Date().toLocaleString())
  }

  ws.onerror = function () {
    reconnect(wsUrl)
    console.log('llws连接错误!')
  }

  ws.onopen = function () {
    heartCheck.reset().start() //心跳检测重置
    console.log('llws连接成功!' + new Date().toLocaleString())
  }

  ws.onmessage = function (event) {
    heartCheck.reset().start()
    const data = JSON.parse(event.data)
    console.log('llws收到消息啦:', data, typeof data)
    if (data.cmd == 'echo') {
      console.log('llws返回pong')
      heartCheck.reset().start() //心跳检测重置
    } else {
      global_callback(data)
    }
  }
}

function reconnect(url) {
  if (lockReconnect) return
  lockReconnect = true
  setTimeout(() => {
    createWebSocket(url)
    lockReconnect = false
  }, 2000)
}

//心跳检测
const heartCheck = {
  timeout: 60000, //1分钟发一次心跳
  timeoutObj: null,
  serverTimeoutObj: null,
  reset: function () {
    clearTimeout(this.timeoutObj)
    clearTimeout(this.serverTimeoutObj)
    return this
  },
  start: function () {
    console.log('开始心跳')
    var self = this
    this.timeoutObj = setTimeout(function () {
      console.log('1分钟到了')
      //这里发送一个心跳，后端收到后，返回一个心跳消息，
      //onmessage拿到返回的心跳就说明连接正常
      const ping = {
        cmd: 'echo',
        seq: 1001,
        t1: Date.now()
      }
      ws.send(JSON.stringify(ping))
      console.log('echo ping!')
      self.serverTimeoutObj = setTimeout(function () {
        //如果超过一定时间还没重置，说明后端主动断开了
        ws.close() //如果onclose会执行reconnect，我们执行ws.close()就行了.如果直接执行reconnect 会触发onclose导致重连两次
      }, self.timeout)
    }, this.timeout)
  }
}
