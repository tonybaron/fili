export function getCookie(cname) {
  //传cookie名称
  let name = cname + '='
  let ca = document.cookie.split(';')
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) == ' ') c = c.substring(1)
    if (c.indexOf(name) != -1) return c.substring(name.length, c.length)
  }
  return ''
}

export function isMobile() {
  let flag = navigator.userAgent.match(
    /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
  )
  return flag
}

export function judgeClient() {
  let u = navigator.userAgent
  let isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1 //判断是否是 android终端
  let isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/) //判断是否是 iOS终端
  console.log('是否是Android：' + isAndroid) //true,false
  console.log('是否是iOS：' + isIOS)
  if (isAndroid) {
    return 'Android'
  } else if (isIOS) {
    return 'IOS'
  } else {
    return 'PC'
  }
}

export const changeDateToArray = (str) => {
  return str.split('-')
}

export const randomNum = (minNum, maxNum) => {
  // eslint-disable-next-line no-undef
  switch (arguments.length) {
    case 1:
      return parseInt(Math.random() * minNum + 1, 10)
    case 2:
      return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10)
    default:
      return 0
  }
}

/**
 * 销毁iframe，释放iframe所占用的内存。
 * @param iframe 需要销毁的iframe id
 */
export function destroyIframe(iframeID) {
  var iframe = document.getElementById(iframeID)

  //把iframe指向空白页面，这样可以释放大部分内存。
  iframe.src = 'about:blank'

  try {
    iframe.contentWindow.document.write('')
    iframe.contentWindow.document.clear()
  } catch (e) {
    console.log(e)
  }

  //把iframe从页面移除
  iframe.parentNode.removeChild(iframe)
}

export function createIframe(
  id,
  url,
  width = '100%',
  height = '100%',
  onLoadCallback,
  timeOut = '2000',
  timeOutCallback
) {
  var timeOutVar = setTimeout(function () {
    clearTimeout(timeOutVar)
    timeOutCallback && timeOutCallback.apply(this, arguments)
    return
  }, timeOut)
  var iframe = document.createElement('iframe')
  iframe.id = id
  iframe.width = width
  iframe.height = height
  iframe.src = url
  if (iframe.attachEvent) {
    iframe.attachEvent('onload', function () {
      clearTimeout(timeOutVar)
      onLoadCallback && onLoadCallback.apply(this, arguments)
    })
  } else {
    iframe.onload = function () {
      clearTimeout(timeOutVar)
      onLoadCallback && onLoadCallback.apply(this, arguments)
    }
  }
  document.body.appendChild(iframe)
  return iframe
}
