import { defineStore } from 'pinia'
import { useStorage } from '@vueuse/core'
import { ApiProfile, ApiPersonalVip, ApiLogout, ApiHomeBanner } from '@api/request'

export const useSystemStore = defineStore('system', {
  state: () => ({
    showiframe: false,
    iframeid: 0,
    isLogin: useStorage('isLogin', false),
    lang: useStorage('lang', 'pt-BR'),
    userAgent: useStorage('agent', 'mobile'),
    profile: useStorage('profile', {}),
    registerInfo: useStorage('registerInfo', {}),
    vipPersonal: useStorage('vipPersonal', {}),
    loginPop: {
      show: false,
      type: 'login'
    },
    showRechagePop: {
      show: false,
      type: 'recharge'
    },
    currencySymbols: {
      'zh-CN': '¥',
      en: '$',
      vi: '$',
      'pt-BR': 'R$'
    },
    withdrawAddPop: false,
    banner: [],
    telegram: useStorage('telegram', ''),
    rotateflag: false
  }),
  getters: {
    telegramurl(state) {
      return 'https://t.me/bbrgol666'
    },
    instagram(state) {
      return 'https://www.instagram.com/bbrgol/'
    },
    userID(state) {
      return state.profile.sysUsername
    },
    currencySymbol(state) {
      return state.currencySymbols[state.lang]
    },
    balance(state) {
      return state.profile.balance
    },
    phonenumber(state) {
      return state.profile.telNumber
    },
    account(state) {
      return state.profile.account
    },
    id(state) {
      return state.profile.id
    },
    sysUsername(state) {
      return state.profile.sysUsername
    },
    viplevel(state) {
      return state.profile.level
    },
    refercode(state) {
      return state.profile.refer
    },
    personbet(state) {
      return state.vipPersonal.bet || []
    },
    persondepo(state) {
      return state.vipPersonal.deposit || []
    },
    withdraw(state) {
      return state.vipPersonal.withdrawable || 0
    },
    inviteLink(state) {
      return `${window.location.origin}/#/home?code=${state.refercode}`
    }
  },
  actions: {
    async getBanner() {
      const res = await ApiHomeBanner()
      if (res.code == 0) {
        this.banner = res.data.banners
        if (res.data.telegram.indexOf('@') != -1) {
          this.telegram = res.data.telegram.slice(1)
        } else {
          this.telegram = res.data.telegram
        }
      }
    },
    async getProfile() {
      this.rotateflag = true
      const res = await ApiProfile()
      console.log('profile', res)
      this.rotateflag = false
      if (res.code == 0) {
        this.profile = res.data
        this.isLogin = true
      }
    },
    async getVipPersonal() {
      const res = await ApiPersonalVip()
      console.log('vip personal', res)
      if (!res) {
        this.isLogin = false
      }
      if (res.code == 0) {
        this.vipPersonal = res.data
        this.isLogin = true
      }
    },
    async logout() {
      const res = await ApiLogout()
      console.log('退出登录', res)
      if (res.code == 0) {
        this.isLogin = false
        this.reset()
      }
    },
    updateBalance(data) {
      console.log('更新用户余额', data)
      this.profile = {
        ...this.profile,
        balance: data
      }
    },
    reset() {
      this.isLogin = false
      this.userAgent = 'mobile'
      this.profile = {}
      this.vipPersonal = {}
      this.lang = 'pt-BR'
    }
  }
})
