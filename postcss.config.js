module.exports = {
  plugins: {
    // postcss-pxtorem 插件的版本需要 >= 5.0.0
    'postcss-pxtorem': {
      rootValue({ file }) {
        return file.indexOf('vant') !== -1 ? 37.5 : 75
      },
      propList: ['*'],
      selectorBlackList: ['el'],
      exclude: function (file) {
        // console.log(file, file.indexOf('element-plus') !== -1)
        return file.indexOf('element-plus') !== -1
      }
    }
  }
}
